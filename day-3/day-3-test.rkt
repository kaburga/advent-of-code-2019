#lang racket

(require "day-3.rkt")

(module+ test
  (require rackunit rackunit/text-ui)


  (define (setup-test-input)
    (make-directory* "test")

    (define out1 (open-output-file "test/input-file-1.txt" #:mode 'text  #:exists 'replace))
    (write-string "R8,U5,L5,D3" out1)
    (newline out1)
    (write-string "U7,R6,D4,L4" out1)
    (close-output-port out1)

    (define out2 (open-output-file "test/input-file-2.txt" #:mode 'text  #:exists 'replace))
    (write-string "R75,D30,R83,U83,L12,D49,R71,U7,L72" out2)
    (newline out2)
    (write-string "U62,R66,U55,R34,D71,R55,D58,R83" out2)
    (close-output-port out2)

    (define out3 (open-output-file "test/input-file-3.txt" #:mode 'text  #:exists 'replace))
    (write-string "R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51" out3)
    (newline out3)
    (write-string "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7" out3)
    (close-output-port out3))

  (define day-3-suite
    (test-suite "Day-3 Tests"

                (test-equal? "Test 1"
                             (day-3 "test/input-file-1.txt")
                             '((part-1 . 6) (part-2 . 30)))

                (test-equal? "Test 2"
                             (day-3 "test/input-file-2.txt")
                             '((part-1 . 159) (part-2 . 610)))

                (test-equal? "Test 3"
                             (day-3 "test/input-file-3.txt")
                             '((part-1 . 135) (part-2 . 410)))))

  (setup-test-input)
  (run-tests day-3-suite) )
