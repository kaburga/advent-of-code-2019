#lang racket

(provide day-4)

(module+ main
  (day-4 "2019_day_4_input.txt"))

(define (day-4 file)
  (define password-range (read-range file))
  (define valid-passwords (find-valid-passwords password-range))
  (define valid-passwords-constr (find-valid-passwords-with-additional-constraint valid-passwords))
  (list (cons 'part-1 (length valid-passwords))
        (cons 'part-2 (length valid-passwords-constr))))

(define (number->digits num)
  (map char->integer (string->list
                      (number->string num))))

(define (find-valid-passwords range)
   (for/list ([potential-password (in-range (first range)
                                            (add1 (second range)))]
              #:when (check-password-validity potential-password))
     potential-password))

(define (find-valid-passwords-with-additional-constraint passwords)
  (for/list ([potential-password passwords]
             #:when (two-same-adjacent potential-password))
    potential-password))

(define (two-same-adjacent passw)
  (define digit-list (number->digits passw))
  (let-values ([(_ group-size found)(for/fold ([prev -1]
                                               [group-size 0]
                                               [found #f])
                                              ([d digit-list])
                                      (cond [(= d prev)
                                             (values d (add1 group-size) found)]
                                            [(= group-size 2)
                                             (values d 1 #t)]
                                            [else
                                             (values d 1 found)]))])
    (or found (= group-size 2))))

(define (check-password-validity pot-passw)
  (define digit-list (number->digits pot-passw))
  (and (= (length digit-list) 6)
       (digits-never-decrease digit-list)
       (same-adjacent digit-list)))

(define (digits-never-decrease dlst)
  (apply <= dlst))

(define (same-adjacent dlst)
  (cond
    [(empty? dlst) #f]
    [(list-prefix? (list (first dlst)) (rest dlst)) #t]
    [else (same-adjacent (rest dlst))]))

(define (read-range file)
  (filter-map string->number
              (regexp-split #px"[-\n]" (file->string file))))
